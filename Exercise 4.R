library(multtest)
data(golub)

a <- as.matrix(rowMeans(golub))#Mean of the data
a_variancE <- var(a)

#0s are ALL while 1s are AML
golub.fac <- factor(golub.cl,levels=0:1,labels=c("ALL","AML"))

#AML and ALL splitted into two
ALL <- ( golub[,golub.fac == "ALL"] )
AML <- (golub[,golub.fac == "AML"])
ALL.mean <- rowMeans(ALL)
AML.mean <- rowMeans(AML)
golub.mean = rowMeans(golub)

#We use sorting to find the maximum five.

ALL.mean.ordered <- order(x = ALL.mean, decreasing = TRUE)
AML.mean.ordered <- order(x = AML.mean, decreasing = TRUE)

ALL.largest <- ALL.mean.ordered[1:5]
AML.largest <- AML.mean.ordered[1:5]

differenceBetweenClasses <- AML.mean-ALL.mean;

sdByRow <- function(x){
  x = as.matrix(x)
  rowCount = dim(x)[1]
  sds = vector(length = rowCount)
  for(i in 1:rowCount){
    sds[i] = sd(x[i,])
  }
  sds
}
differenceBetweenClasses.ordered <- order( x = differenceBetweenClasses, decreasing = TRUE )

differenceBetweenClasses.largest <- differenceBetweenClasses.ordered[1:5]
differenceBetweenClasses.largest.names <- 
  as.array(golub.gnames[differenceBetweenClasses.largest,2])

differenceBetweenClasses.ordered.sd <- sdByRow( golub[differenceBetweenClasses.largest,] )
differenceBetweenClasses.golub.mean <- as.array(golub.mean[differenceBetweenClasses.largest])
differenceBetweenClasses.ALL.mean <- as.array(ALL.mean[differenceBetweenClasses.largest])
differenceBetweenClasses.AML.mean <- as.array(AML.mean[differenceBetweenClasses.largest])

iibiv = data.frame(Names = differenceBetweenClasses.largest.names,
           StandardDeviation = differenceBetweenClasses.ordered.sd,
           Mean = differenceBetweenClasses.golub.mean,
           ALLMean = differenceBetweenClasses.ALL.mean,
           AMLMean = differenceBetweenClasses.AML.mean
           )

write.csv(x = iibiv, file = "2b.iv.csv")


#c ------------------

ccndd3 <- golub[1042,]

ccndd3.ALL <- (ccndd3[golub.fac == "ALL"])
ccndd3.AML <- (ccndd3[golub.fac == "AML"])

#2.c.i -----
boxplot(ccndd3.AML, ccndd3.ALL);
#plot.new()

#2.c.ii -----
qqnorm(ccndd3.ALL, main = "Q-Q Plot For ALL CCNDD3")
qqline(ccndd3.ALL)

qqnorm(ccndd3.AML, main = "Q-Q Plot For AML CCNDD3")
qqline(ccndd3.AML)

#2.c.iii ------ 
#We use t.test assuming it is a normal distribution

ccndd3.test.t <- t.test(ccndd3.AML, ccndd3.ALL)
print(ccndd3.test.t)

#2.c.iv ----
#We use Wilcox test as it doesn't matter for it if the distributions are normal
print("And now wilcox")
ccndd3.test.wilcox <- wilcox.test(ccndd3.AML, ccndd3.ALL)
print(ccndd3.test.wilcox)
#Both of them say they are different distributions for a significance level 0.01;
#therefore they are highly significant

#2.d

rowCount = dim(ALL)[1]
golub.t.test <- array(dim = rowCount)

for(i in 1:rowCount){
  golub.t.test <- t.test(AML[i,], ALL[i,])
}

#2.d.i

